function createNewUser() {
  const newUser = {
    firstName: prompt("Введіть своє ім'я:"),
    lastName: prompt("Введіть своє прізвище:"),
    getLogin() {
      return (
        this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
      );
    },
  };

  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
